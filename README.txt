Module: "Video Filter: Twitch"
Author: Sergey Fayngold

DESCRIPTION
-------
This module adds support for Twitch.tv to the Video Filter project

REQUIREMENTS
-------
- Video Filter module

INSTALL
-------
1) Install Video Filter from http://drupal.org/project/video_filter
2) Drop this modul into a subdir of your choice (regarding the Drupal standards)
    for example sites/all/modules
3) Enable the module under admin/modules
4) Enjoy =)
